package main

import (
	"log"
	"net/http"
	"strconv"
)

var counter int = 0

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(strconv.Itoa(counter) + "\n"))
		log.Println(counter)
		counter++
	})

	log.Fatalln(http.ListenAndServe(":12180", nil))
}
