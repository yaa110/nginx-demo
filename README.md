Nginx Demo
==========
It should be placed in your `GOPATH` directory.

- `server`: starts nginx service and HTTP server
- `client`: sends `GET` requests directly to HTTP server every one second
- `client_cache`: sends `GET` requests to nginx every one second, a querystring parameter of `cache=3s` is set for the request.

[![asciicast](https://asciinema.org/a/lsFAfbE2dexQaXGeRsduVNEwx.png)](https://asciinema.org/a/lsFAfbE2dexQaXGeRsduVNEwx)

